<!DOCTYPE HTML> 
<html>
<head>
<title> PHP Calculator </title>
</head>
<body> 

<form method="POST">
<fieldset>
<table align = "center"> 
	<legend>PHP Calculator</legend>
	<br>
	<label>Enter 1st Number: </label>
	<input type="text" name="val1" value="" />
	<br>
	<br>
	<label>Operator: </label>
	<input type="radio" name="method" id="addition" value ="1"> +
	<input type="radio" name="method" id="subtraction" value ="2"> -
	<input type="radio" name="method" id="multiplication" value ="3"> *
	<input type="radio" name="method" id="division" value = "4"> /
	<br>
	<br>
	<label>Enter 2nd Number: </label>
	<input type="text" name="val2" value="" />
	
	<br>
	<br>
	<input type="submit" class="button" name="equal" value="Compute" />

</fieldset>
</form>

<?php
	$output = "";
if (isset($_POST["equal"])){

	$val1 = $_POST["val1"];
	$val2 = $_POST["val2"];
	if (empty($_POST["method"])) {
		$method = 0;
	} else {
		$method = $_POST["method"];
	}
	$val1 = trim($val1);
	$val2 = trim($val2);
	
	$the_reset = ' <a href ="'.htmlentities($_SERVER["REQUEST_URI"]).'">Reset</a>';


	if($method == 1) {
		$theanswer = $val1 + $val2;	
	} else if ($method == 2) {
		$theanswer = $val1 - $val2;
	} else if ($method == 3) {
		$theanswer = $val1 * $val2;
	} else if ($method == 4 && $val2 != 0) {
		$theanswer = $val1 / $val2;
	}
	
	if (strlen($val1) > 10 || strlen($val2) > 10) {
		$output = '<p class="error"><strong>Error:</strong> 
		Input limit exceeded.</p>';
	} else if ($method == 4 && $val2 == 0) {
		$output = ' <p class="error"><strong>Error:</strong>
		Division by zero is not allowed.</p>';
	} else if (is_null($val1)) {
        $output = ' <p class="error"><strong>Error:</strong> 
        Please enter first value.</p>';
    }else if (is_null($val2)) {
		$output = ' <p class="error"><strong>Error:</strong>
		Please enter second value.</p>';
	} else if (!is_numeric($val1)|| !is_numeric($val2)) {
		$output = ' <p class="error"><strong>Error:</strong> Numbers only.</p>';
	} else if ($method == 0) {
		$output = ' <p class="error"><strong>Error:</strong>
		Please choose an operator.</p>';
	} else {
		$output = ' <p><strong>The answer is: </strong>'.$theanswer.'</p>';
	}
	echo $output;
}

?>
</body>
</html>
